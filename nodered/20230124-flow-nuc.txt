[
    {
        "id": "b8d5b45faf9e3f7e",
        "type": "tab",
        "label": "config",
        "disabled": false,
        "info": "",
        "env": []
    },
    {
        "id": "a3e1ee3335202439",
        "type": "inject",
        "z": "b8d5b45faf9e3f7e",
        "name": "on-start",
        "props": [
            {
                "p": "payload"
            },
            {
                "p": "topic",
                "vt": "str"
            }
        ],
        "repeat": "",
        "crontab": "",
        "once": true,
        "onceDelay": 0.1,
        "topic": "",
        "payload": "config loaded...",
        "payloadType": "str",
        "x": 140,
        "y": 100,
        "wires": [
            [
                "1a3dd15586f399db",
                "212d96996fa4776c"
            ]
        ]
    },
    {
        "id": "1ae42859b2cd2590",
        "type": "comment",
        "z": "b8d5b45faf9e3f7e",
        "name": "NUC device",
        "info": "",
        "x": 150,
        "y": 60,
        "wires": []
    },
    {
        "id": "1a3dd15586f399db",
        "type": "debug",
        "z": "b8d5b45faf9e3f7e",
        "name": "on-start",
        "active": true,
        "tosidebar": true,
        "console": false,
        "tostatus": false,
        "complete": "payload",
        "targetType": "msg",
        "statusVal": "",
        "statusType": "auto",
        "x": 300,
        "y": 100,
        "wires": []
    },
    {
        "id": "212d96996fa4776c",
        "type": "function",
        "z": "b8d5b45faf9e3f7e",
        "name": "config",
        "func": "\nvar dev = {\n    id: 'nuc'\n};\nvar svc = {\n    hub: {\n        url: 'http://192.168.1.2:1880',\n        ip: '192.168.1.2'\n    },\n    esb: {\n        url: 'http://192.168.1.2:9880'\n    },\n    reminder: {\n        url: 'http://192.168.1.2:9017'\n    },\n    nuc: {\n        url: \"http://localhost:1880\",\n        ip: '192.168.1.2'\n    }\n};\n\nvar main_dir = '/home/pi';\nvar dir = {\n    main: main_dir\n};\n\nglobal.set('device', dev);\nglobal.set('svc', svc);\nglobal.set('dir', dir);\n\nreturn msg;\n\n\n",
        "outputs": 1,
        "noerr": 0,
        "initialize": "",
        "finalize": "",
        "libs": [],
        "x": 290,
        "y": 140,
        "wires": [
            []
        ]
    }
]