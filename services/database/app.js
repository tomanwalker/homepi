
// ## dependencies

var dba = require('./lib/dba');

// ## config
var testTable = 'test';

// ## func


// ## flow
var tables = dba.listTables();
console.log( 'tables = %j', tables );

var docs = dba.listDocuments(testTable);
console.log( 'docs[%s] = %j', testTable, docs );

dba.insertDoc(testTable, { _id: '123', here: 'abc' });
dba.search(testTable, {here: 'abc' });
dba.index(testTable);

setTimeout(function(){
	dba.insertDoc(testTable, { here: 'not-good' });
	dba.index(testTable);
}, 20);

setTimeout(function(){
	
	var result = dba.search(testTable, {here: 'abc' });
	console.log( 'search - result = %j', result );
	
}, 50);



