
// ## dependencies
var fs = require('fs');

var utils = require('./utils');

// ## exp
var ns = {};
module.exports = ns;

// ## config
var FILE_EXT = '.json';
var folder = './data';

var indexHolder = {};

// ## fund
ns.listTables = function(){
	var arr = fs.readdirSync(folder);
	return arr.map(x => x.replace('.db', ''));
};

ns.listDocuments = function(tableName){
	var arr = fs.readdirSync(folder + '/' + tableName);
	return arr.map(x => x.replace(FILE_EXT, ''));
};

ns.insertDoc = function(tableName, doc){
	var str = JSON.stringify(doc, null, 2);
	var id = doc._id;
	if( !id ){
		var id = utils.makeId();
		doc._id = id;
	}
	
	var filePath = folder + '/' + tableName + '/' + id + FILE_EXT;
	return fs.writeFileSync(filePath, str);
};

ns.getDoc = function(tableName, id){
	var filePath = folder + '/' + tableName + '/' + id + FILE_EXT;
	var txt = fs.readFileSync( filePath );
	return JSON.parse(txt);
};

ns.searchFull = function(tableName, query){
	var q_keys = Object.keys(query);
	var doc_list = ns.listDocuments(tableName);
	var docs = doc_list.map( x => ns.getDoc(tableName, x) );
	
	var result = {
		total_rows: docs.length,
		rows: docs
	};
	
	if( q_keys.length === 0 ){
		return result;
	}
	
	var found = docs.filter(function(x){
		
		var match = q_keys.filter(function(z){
			return query[z] === x[z];
		});
		
		return match.length === q_keys.length;
	});
	
	result.total_rows = found.length;
	result.rows = found;
	return result;
};
ns.searchIndex = function(tableName, query){
	
	var tableIndex = indexHolder[tableName];
	var q_keys = Object.keys(query);
	if( q_keys.length === 0 ){
		return ns.searchFull(tableName, query);
	}
	
	var id_list = [];
	
	q_keys.forEach(function(p){
		var searchVal = query[p];
		
		if( tableIndex[p] && tableIndex[p][searchVal] ){
			var pointers = tableIndex[p][searchVal];
			id_list = id_list.concat( pointers.map(x => x.id) );
		}
		
	});
	
	var result = {
		total_rows: 0,
		rows: []
	};
	if( id_list.length === 0 ){
		return result;
	}
	
	id_list.sort();
	id_list = id_list.filter(function(item, pos) {
		return id_list.indexOf(item) == pos;
	});
	
	result.total_rows = id_list.length;
	result.rows = id_list.map( x => ns.getDoc(tableName, x) );
	return result;
};
ns.search = function(tableName, query){
	
	var start = new Date();
	var result = null;
	var exists = typeof(indexHolder[tableName]) !== 'undefined';
	
	if( exists ){
		result = ns.searchIndex(tableName, query);
	}
	else {
		result = ns.searchFull(tableName, query);
	}
	
	var diff = (new Date()) - start;
	console.log('dba.search - end - table = %s | index = %s | found = %s | time = %s ms', 
		tableName, exists, result.total_rows, diff);
	return result;
	
};

ns.indexFull = function(tableName){
	// async
	setTimeout(function(){
		
		var start = new Date();
		console.log('dba.index - start - table = %s', tableName);
		
		if( !indexHolder[tableName] ){
			indexHolder[tableName] = {};
		}
		
		var tableIndex = indexHolder[tableName];
		var date_iso = utils.timestamp();
		
		var docList = ns.listDocuments(tableName);
		console.log('dba.index - list - table = %s / %s', tableName, docList.length);
		
		docList.forEach(function(id){
			var doc = ns.getDoc(tableName, id);
			for(var p in doc){
				if( utils.isPrimitive(doc[p]) && p !== '_id' ){
					
					var propName = p;
					var propVal = doc[p];
					
					if( !tableIndex[propName] ){
						tableIndex[propName] = {};
					}
					if( !tableIndex[propName][propVal] ){
						tableIndex[propName][propVal] = [];
					}
					
					if( !tableIndex[propName][propVal].find( x => x.id === id ) ){
						var obj = {id: id, ts: date_iso};
						tableIndex[propName][propVal].push(obj);
					}
					
				}
			}
		});
		
		var diff = (new Date()) - start;
		console.log('dba.index - ready - table = %s | diff = %s | r = %j', tableName, diff, tableIndex);
		
	}, 10);
};

ns.indexPartial = function(tableName){
	// async
	setTimeout(function(){
		var start = new Date();
		console.log('dba.indexPartial - start - table = %s', tableName);
		var tableIndex = indexHolder[tableName];
		var date_iso = utils.timestamp();
		
		var docList = ns.listDocuments(tableName);
		console.log('dba.indexPartial - list - table = %s / %s', tableName, docList.length);
		
		// How to identify Old, New?
		// Need to add Meta obj to IndexHolder
		
		
		var diff = (new Date()) - start;
		console.log('dba.index - ready - table = %s | diff = %s | r = %j', tableName, diff, tableIndex);
		
	}, 10);
};

ns.index = function(tableName){
	
	return ns.indexFull(tableName);
};


