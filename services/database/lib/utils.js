
// ## dependencies

// ## exp
var ns = {};
module.exports = ns;

// ## config

// ## fund 
ns.randomBetween = function (min, max) { 
	// min and max included 
	return Math.floor(Math.random() * (max - min + 1) + min)
};

ns.timestamp = function(date){
	if( !date ){
		date = new Date();
	}
	return date.toISOString();
};

ns.makeId = function(){
	var date_iso = ns.timestamp();
	var ts = date_iso.slice(0,19).replace(/\-/g, '').replace('T', '-').replace(/\:/g, '');
	return (ts + '-' + ns.randomBetween(1000, 9999));
};

ns.isPrimitive = function(val){
	if(val === Object(val)){
		return false;
	}
	return true;
};


