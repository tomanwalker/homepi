
## changedetection
### cd-selenium
docker run -d -p 4444:4444 -p 5900:5900 -p 7900:7900 \
--name selenium --net=dev \
--restart=unless-stopped --memory=512m \
-e FETCH_WORKERS=4 --shm-size="2g" \
-e SE_NODE_MAX_SESSIONS=10 -e SE_NODE_OVERRIDE_MAX_SESSIONS=true \
--label tier=middle \
selenium/standalone-chrome:4.7.2-20221219

### cd-ui
docker run -d -p 9005:5000 \
--name changedetect --net=dev \
--restart=unless-stopped --memory=384m \
-e WEBDRIVER_URL="http://selenium.dev:4444/wd/hub" \
-v $HOME/docker/changedetect:/datastore \
--label tier=app --label depends=selenium \
dgtlmoon/changedetection.io

## mysql
docker run -d -p 3306:3306 \
--name mysql --net=dev \
--restart=unless-stopped --memory=1024m \
-v $HOME/docker/mysql:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=123 \
--label tier=middle \
mysql:8

## phpadmin - 192.168.1.3:9006
docker run -d -p 9006:80 \
--name myadmin --net=dev \
--restart=unless-stopped --memory=256m \
-e PMA_ARBITRARY=1 \
--label tier=app --label depends=mysql \
phpmyadmin







