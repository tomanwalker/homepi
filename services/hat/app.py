from sense_hat import SenseHat
import paho.mqtt.client as mqtt
import time
import json

## pip install -r req.txt
## https://github.com/raspberrypilearning/astro-pi-guide/blob/master/inputs-outputs/led-matrix.md

## Config
sense = SenseHat()
client = mqtt.Client(client_id="hat", clean_session=False)

### Fund
def get_temp():
    obj = {
        "temperature": round(sense.get_temperature(), 2),
        "humidity": round(sense.get_humidity(), 2),
        "pressure": round(sense.get_pressure(), 2)
    }
    
    #print(obj)
    return obj
#end def-temp

def get_stick():
    
    '''
    [InputEvent(timestamp=1657093514.356324, direction='left', action='pressed'), InputEvent(timestamp=1657093514.528653, direction='left', action='released')]
    [InputEvent(timestamp=1657093517.38599, direction='right', action='pressed'), InputEvent(timestamp=1657093517.558944, direction='right', action='released')]
    [InputEvent(timestamp=1657093519.77005, direction='middle', action='pressed'), InputEvent(timestamp=1657093519.870516, direction='middle', action='released')]
    '''
    event = sense.stick.get_events()
    #print(event)
    
    if len(event) == 0:
        return False
    
    state_map = {
        'released': 0,
        'pressed': 1,
        'held': 2
    }
    key = event[0].direction.upper()
    if key == 'MIDDLE':
        key = 'ENTER'
    
    obj = {
        "key": key,
        "state": state_map[event[0].action]
    }
    
    print(obj)
    return obj
#end-def-stick

def coor(dot):
    
    arr = [0]
    if dot.isdigit():
        arr = [int(dot)]
    if dot == '*':
        arr = range(8)
    if '-' in dot:
        sp = dot.split('-')
        arr = range(int(sp[0]), int(sp[1])+1)
    
    return arr
#end-def-coor

def hex_to_rgb(hex):
    rgb = []
    for i in (0, 2, 4):
        decimal = int(hex[i:i+2], 16)
        rgb.append(decimal)
  #print(hex_to_rgb('FFA501'))
    return rgb
#end-def-hex

def set_led(txt):
    ## In:
    ## *,*,black, 0-3,6-7,#277573
    ## Out:
    ## sense.set_pixel(x, y, 255, 255, 255)
    
    color_map = {
        'black': [0, 0, 0],
        'off': [0, 0, 0],
        'white': [255, 255, 255],
        'purple': [128,0,128],
        '#b02121': [156, 48, 64],
        '#b02121\nD0': [156, 48, 64]
    }
    
    sp = txt.split(',')
    print('debug sp = ', sp)
    
    for i in range(0, len(sp), 3):
        #print(i)
        print('i = ', i, ' 0 = ', sp[i], ' 1 = ', sp[i+1], ' 2 = ', sp[i+2])
        
        x = coor(sp[i])
        y = coor(sp[i+1])
        c = color_map['off']
        
        if '#' in sp[i+2]:
            c = hex_to_rgb(sp[i+2].lstrip('#'))
        if sp[i+2] in color_map:
            c = color_map[sp[i+2]]
        
        print('led - x = ', x, ' y = ', y, ' c = ', c)
        for h in x:
            for v in y:
                sense.set_pixel(h, v, c[0], c[1], c[2])
        
    #end-for
    
#end-def-led

def on_message(client, userdata, message):
    
    print("message topic=", message.topic)
    txt = str(message.payload.decode("utf-8"))
    print("message pay = ", txt)
    #obj =  json.load(txt)
    #print(obj)
    
    if message.topic == "cmd/hat/led":
        set_led(txt)
    
#end-def-mess

### Flow
sense.clear()

client.connect('localhost', keepalive=10)
client.on_message=on_message
client.subscribe("cmd/hat/+")

client.loop_start()
cnt = 0

while True:
    
    stick = get_stick()
    if stick:
        client.publish("evt/hat/stick", json.dumps(stick))
    
    if cnt % 2 == 0:
        client.publish("evt/hat/temp", json.dumps(get_temp()))
        
    #end-if
    
    cnt += 1
    if cnt > 100:
        cnt=0
    time.sleep(0.5)
    
#end While


