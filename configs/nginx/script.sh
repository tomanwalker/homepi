
ssh pi@192.168.1.3

## install

python --version || python3 --version

sudo apt update
sudo apt upgrade -y
sudo apt install certbot
sudo apt install python3-certbot-nginx

## config
sudo nano /etc/nginx/sites-available/portfolio
# portfolio
### site-k

sudo mkdir -p /var/www/certbot

# symlink
sudo ln -s /etc/nginx/sites-available/portfolio /etc/nginx/sites-enabled/
sudo rm /etc/nginx/sites-enabled/default

# verify and restart
sudo nginx -t && sudo nginx -s reload

## SSL
sudo certbot certonly --webroot -w /var/www/certbot \
-d alexkivekas.com -d www.alexkivekas.com

sudo certbot certonly --webroot -w /var/www/certbot \
-d sportdiv.alexkivekas.com

sudo certbot certonly --webroot -w /var/www/certbot \
-d home.alexkivekas.com -d nextcloud.home.alexkivekas.com \
-d next.home.alexkivekas.com

sudo certbot certonly --webroot -w /var/www/certbot \
-d books.home.alexkivekas.com

## SSL Wildcard
sudo certbot --server https://acme-v02.api.letsencrypt.org/directory \
-d alexkivekas.com -d *.alexkivekas.com -d *.home.alexkivekas.com \
--manual --preferred-challenges dns-01 certonly

sudo certbot certificates

sudo certbot renew

sudo certbot delete

## links
https://www.sitepoint.com/configuring-nginx-ssl-node-js/
https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/

## Cron
crontab -e

0 12 * * * /usr/bin/certbot renew --quiet

crontab -l



