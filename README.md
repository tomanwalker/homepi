
# homepi
Home digital assistant solution

------------------
HW: 
- Raspberry pi
- Sense HAT
- USB Stick (for backup)
   
SW-stack: 
- Node-red
- Docker
- Nginx (certbot)
   
Desktop-apps: SSH   
Integrations: Trello, Dropbox, Discord   
Mobile apps: Trello, Discord   

-----------

![overview](https://gitlab.com/tomanwalker/homepi/-/raw/master/overview-2022.png "")

## == INSTALLATION ==
### general
```shell
sudo apt update && sudo apt -y upgrade

# Raspi - turn off swap
dphys-swapfile swapoff
dphys-swapfile uninstall

## increase swap
sudo dphys-swapfile swapoff
sudo nano /etc/dphys-swapfile
sudo dphys-swapfile setup
sudo dphys-swapfile swapon

# Raspi - video memory ~ 128 MB
# Raspi - turn ON - ssh / vnc
# Preferably - on Router [DHCP] set Static IP, so that RPi always has the same local IP

# K8S / docker change
cgroup_memory=1 cgroup_enable=memory 

look at:
- configs/docker.txt
- configs/nginx/script.sh

```

### usb (kodi)
```
## check connected
df

## automaount under /media

```

### drive (kodi)
```
sudo blkid
sudo mkdir -p /mnt/usb
sudo chown -R pi:pi /mnt/usb

sudo nano /etc/fstab
## FAT
/dev/sda5       /mnt/usb vfat uid=pi,gid=pi 0 0

#-NTFS-
/dev/sda5       /mnt/mydrive    ntfs-3g permissions,locale=en_US.utf8   0       2

# reboot and check drive permsissions
sudo reboot now
ls -l /mnt
```

### Node-red Install-
```shell
#preinstalled on Raspi
#for other -> nodejs + npm

node -v
sudo npm install -g node-red
```

### NFS
Need to share "/media" Folder

```
## Server

## Client
mount -t nfs -o proto=tcp,port=2049 192.168.1.3:/ /mnt

```

### samba (kodi)
```shell
sudo cp smb.conf /etc/samba/smb.conf
```

### Transmission (kodi)
```shell
sudo apt install transmission-daemon

sudo cp transmission/torrent_notice /home/pi/dev/torrent_notice
sudo cp transmission/settings.json /etc/transmission-daemon/settings.json
```

### Kodi
https://kodi.wiki/view/HOW-TO:Install_Kodi_for_Linux



